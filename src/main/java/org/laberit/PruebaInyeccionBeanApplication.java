package org.laberit;

import org.laberit.repository.IProducto;
import org.laberit.repository.ProductoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaInyeccionBeanApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(PruebaInyeccionBeanApplication.class, args);
	}

	@Autowired
	IProducto producto;
	
	@Override
	public void run(String... args) throws Exception {
		System.out.println("Esto es una prueba de inyección.");
		System.out.println(producto.registrar("Coche"));
		
	}

}
